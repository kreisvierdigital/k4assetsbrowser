# k4 Assets Browser plugin for Craft CMS 3.x

Browse Assets 

![Screenshot](resources/img/plugin-logo.png)

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require /k4-assets-browser

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for k4 Assets Browser.

## k4 Assets Browser Overview

-Insert text here-

## Configuring k4 Assets Browser

-Insert text here-

## Using k4 Assets Browser

-Insert text here-

## k4 Assets Browser Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Christian Hiller](https://kreisvier.ch)
