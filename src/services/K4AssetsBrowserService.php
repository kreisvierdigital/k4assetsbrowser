<?php
/**
 * k4 Assets Browser plugin for Craft CMS 3.x
 *
 * Browse Assets 
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4assetsbrowser\services;

use craft\elements\Asset;
use craft\helpers\UrlHelper;
use k4\k4assetsbrowser\K4AssetsBrowser;

use Craft;
use craft\base\Component;
use k4\k4assetsbrowser\models\K4AssetsBrowserModel;
use Kint\Kint;

/**
 * @author    Christian Hiller
 * @package   K4AssetsBrowser
 * @since     1.0.0
 */
class K4AssetsBrowserService extends Component
{
    // Public Methods
    // =========================================================================

    public function getFolders($folderId)
    {
        $folder = Craft::$app->assets->findFolder(array('id' => $folderId));
        return $folder->getChildren();
    }

    public function getFiles($folderId)
    {
        $files = Asset::find()->anyStatus()->folderId($folderId)->all();
        return $files;
    }

    public function getEntriesBySource($assetSourceId)
    {
        $tree = Craft::$app->assets->getFolderTreeByVolumeIds($assetSourceId);
        return $this->getEntries($tree[0]->id);
    }

    public function getEntries($folderId, $baseFolderId = null)
    {
        $entries = array();
        $requestUrl = strtok(Craft::$app->request->url, '?'); // strip parameter

        $basefolder = Craft::$app->assets->findFolder(array('id' => $folderId));

        // the user needs permission to view the asset
        $permission = 'viewassetsource';

        // check if the current user has the required permissions
        $hasAccess = Craft::$app->getUser()->checkPermission($permission . ':' . $basefolder->getVolume()->id);

        if ($hasAccess && $basefolder){

            if ($folderId != $baseFolderId && $basefolder->getParent())
            {
                $assetsBrowserModel = new K4AssetsBrowserModel();
                $assetsBrowserModel->name       = "...";
                $assetsBrowserModel->filename   = "...";
                $assetsBrowserModel->url        = $this->getBrowserFolderLink($basefolder->getParent()->id);
                $assetsBrowserModel->kind       = 'folder';
                //k4 add
                $assetsBrowserModel->id         = $basefolder->getParent()->id;
                $assetsBrowserModel->title      = "";
                $assetsBrowserModel->size       = "";
                $assetsBrowserModel->copyrightInformationenIntranet = "";
                $assetsBrowserModel->fileDescription = "";
                //k4 add
                $entries[] = $assetsBrowserModel;
            }
            $folders = $this->getFolders($folderId);
            foreach ($folders as $folder)
            {
                // Hack K4, Exclude Antrag-Ornders
                //if ($folder->id != 109 && $folder->id != 111 && $folder->id != 113 && $folder->id != 128) {
                $assetsBrowserModel = new K4AssetsBrowserModel();
                $assetsBrowserModel->name       = $folder->name;
                $assetsBrowserModel->filename   = $folder->name;
                $assetsBrowserModel->url        = $this->getBrowserFolderLink($folder->id);
                $assetsBrowserModel->kind       = 'folder';
                //k4 add
                $assetsBrowserModel->id         = $folder->id;
                $assetsBrowserModel->size       = "";
                $assetsBrowserModel->title      = "";
                $assetsBrowserModel->copyrightInformationenIntranet = "";
                $assetsBrowserModel->fileDescription = "";
                //k4 add
                $entries[] = $assetsBrowserModel;
                //}
            }

            $files = $this->getFiles($folderId);
            foreach ($files as $file)
            {
                $assetsBrowserModel = new K4AssetsBrowserModel();
                $assetsBrowserModel->name       = $file->filename;
                $assetsBrowserModel->filename   = $file->filename;
                $assetsBrowserModel->url        = $file->url;
                $assetsBrowserModel->kind       = $file->kind;
                //k4 add
                $assetsBrowserModel->id         = $file->id;
                $assetsBrowserModel->title      = $file->title;
                $assetsBrowserModel->size       = $file->size;
                $assetsBrowserModel->copyrightInformationenIntranet = $file->copyrightInformationenIntranet;
                $assetsBrowserModel->fileDescription                = $file->fileDescription;
                //k4 add
                $entries[] = $assetsBrowserModel;
            }

        }



        return $entries;
    }

    public function getBrowserFolderLink($folderId)
    {
        $requestUrl = strtok(Craft::$app->request->url, '?'); // strip parameter
        return UrlHelper::urlWithParams($requestUrl, array('folderId' => $folderId));
    }

    public function breadcrumbs($number, $baseFolderId = null)
    {
        $folderobjects = array();
        return $this->breadcrumb_internal($folderobjects, $number, $baseFolderId);
    }

    // Modified from https://github.com/brammittendorff/assetbreadcrumb
    private function breadcrumb_internal($folderobjects, $folderId, $baseFolderId = null)
    {
        $folder = Craft::$app->assets->getFolderById((int)$folderId);
        if(!empty($folder->parentId) && $folderId != $baseFolderId) {
            $folderobjects[] = $folder;
            return $this->breadcrumb_internal($folderobjects, $folder->parentId);
        }
        elseif($baseFolderId) {
            $folderobjects[] = Craft::$app->assets->getFolderById((int)$baseFolderId);
            return $folderobjects;
        }
        else {
            return array_reverse($folderobjects);
        }
    }
}
