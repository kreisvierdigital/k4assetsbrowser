<?php
/**
 * k4 Assets Browser plugin for Craft CMS 3.x
 *
 * Browse Assets 
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4assetsbrowser\models;

use k4\k4assetsbrowser\K4AssetsBrowser;

use Craft;
use craft\base\Model;

/**
 * @author    Christian Hiller
 * @package   K4AssetsBrowser
 * @since     1.0.0
 */
class K4AssetsBrowserModel extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $kind;

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $size;

    /**
     * @var string
     */
    public $copyrightInformationenIntranet;

    /**
     * @var string
     */
    public $fileDescription;


    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['name','filename','url','kind','id','title','size','copyrightInformationenIntranet','fileDescription'], 'string'],

        ];
    }
}
