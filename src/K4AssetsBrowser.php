<?php
/**
 * k4 Assets Browser plugin for Craft CMS 3.x
 *
 * Browse Assets 
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4assetsbrowser;

use k4\k4assetsbrowser\services\K4AssetsBrowserService;
use k4\k4assetsbrowser\variables\K4AssetsBrowserVariable;
use k4\k4assetsbrowser\models\Settings;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\twig\variables\CraftVariable;

use yii\base\Event;

/**
 * Class K4AssetsBrowser
 *
 * @author    Christian Hiller
 * @package   K4AssetsBrowser
 * @since     1.0.0
 *
 * @property  K4AssetsBrowserService $k4AssetsBrowserService
 */
class K4AssetsBrowser extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var K4AssetsBrowser
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public string $schemaVersion = '1.0.0';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        $this->initLogging();
        $this->initTwigVariables();
        $this->initServices();

    }

    // Protected Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    protected function createSettingsModel(): ?\craft\base\Model
    {
        return new Settings();
    }

    /**
     * @inheritdoc
     */
    protected function settingsHtml(): string
    {
        return Craft::$app->view->renderTemplate(
            'k4-assets-browser/settings',
            [
                'settings' => $this->getSettings()
            ]
        );
    }

    private function initTwigVariables()
    {
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('k4AssetsBrowser', K4AssetsBrowserVariable::class);
            }
        );
    }

    private function initLogging()
    {
        Craft::info(
            Craft::t(
                'k4-assets-browser',
                '{name} plugin loaded',
                ['name' => 'K4 Assets Browser']
            ),
            __METHOD__
        );
    }

    private function initServices()
    {
        $this->setComponents([
            'data' => K4AssetsBrowserService::class,
        ]);
    }


}
