<?php
/**
 * k4 Assets Browser plugin for Craft CMS 3.x
 *
 * Browse Assets 
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4assetsbrowser\variables;


use k4\k4assetsbrowser\K4AssetsBrowser;
use Craft;
use k4\k4assetsbrowser\services\K4AssetsBrowserService;

/**
 * @author    Christian Hiller
 * @package   K4AssetsBrowser
 * @since     1.0.0
 */
class K4AssetsBrowserVariable
{

    /**
     * @return K4AssetsBrowserService
     */
    private function getService(){
        return K4AssetsBrowser::getInstance()->data;
    }

    // Public Methods
    // =========================================================================

    public function source($assetSourceId)
    {
        $folderId = Craft::$app->request->getParam('folderId');
        if ($folderId)
            return $this->getService()->getEntries($folderId);
        else
            return $this->getService()->getEntriesBySource($assetSourceId);
    }

    public function folder($folderId)
    {
        $currentFolderId = Craft::$app->request->getParam('folderId');
        if ($currentFolderId)
            return $this->getService()->getEntries($currentFolderId, $folderId);
        else
            return $this->getService()->getEntries($folderId, $folderId);
    }

    public function folderUrl($folderId)
    {
        return $this->getService()->getBrowserFolderLink($folderId);
    }

    public function breadcrumbsSource($assetSourceId)
    {
        $folderId = Craft::$app->request->getParam('folderId');
        if ($folderId)
            return $this->getService()->breadcrumbs($folderId);
        else
        {
            $tree = Craft::$app->assets->getFolderTreeByVolumeIds($assetSourceId);
            return $this->getService()->breadcrumbs($tree[0]->id);
        }
    }

    public function breadcrumbsFolder($folderId)
    {
        $currentFolderId = Craft::$app->request->getParam('folderId');
        if ($currentFolderId)
            return $this->getService()->breadcrumbs($currentFolderId, $folderId);
        else
            return $this->getService()->breadcrumbs($folderId);
    }

}
