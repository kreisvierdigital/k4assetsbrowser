<?php
/**
 * k4 Assets Browser plugin for Craft CMS 3.x
 *
 * Browse Assets 
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

/**
 * k4 Assets Browser config.php
 *
 * This file exists only as a template for the k4 Assets Browser settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'k4-assets-browser.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

return [

    // This controls blah blah blah
    "someAttribute" => true,

];
