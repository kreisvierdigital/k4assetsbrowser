<?php
/**
 * k4 Assets Browser plugin for Craft CMS 3.x
 *
 * Browse Assets 
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

namespace k4\k4assetsbrowser\assetbundles\k4assetsbrowser;

use Craft;
use craft\web\AssetBundle;
use craft\web\assets\cp\CpAsset;

/**
 * @author    Christian Hiller
 * @package   K4AssetsBrowser
 * @since     1.0.0
 */
class K4AssetsBrowserAsset extends AssetBundle
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = "@k4/k4assetsbrowser/assetbundles/k4assetsbrowser/dist";

        $this->depends = [
            CpAsset::class,
        ];

        $this->js = [
            'js/K4AssetsBrowser.js',
        ];

        $this->css = [
            'css/K4AssetsBrowser.css',
        ];

        parent::init();
    }
}
