/**
 * k4 Assets Browser plugin for Craft CMS
 *
 * k4 Assets Browser JS
 *
 * @author    Christian Hiller
 * @copyright Copyright (c) 2019 Christian Hiller
 * @link      https://kreisvier.ch
 * @package   K4AssetsBrowser
 * @since     1.0.0
 */
