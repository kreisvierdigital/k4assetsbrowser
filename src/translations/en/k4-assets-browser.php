<?php
/**
 * k4 Assets Browser plugin for Craft CMS 3.x
 *
 * Browse Assets 
 *
 * @link      https://kreisvier.ch
 * @copyright Copyright (c) 2019 Christian Hiller
 */

/**
 * @author    Christian Hiller
 * @package   K4AssetsBrowser
 * @since     1.0.0
 */
return [
    'k4 Assets Browser plugin loaded' => 'k4 Assets Browser plugin loaded',
];
